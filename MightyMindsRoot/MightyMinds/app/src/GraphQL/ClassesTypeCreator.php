<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;
use GraphQL\Type\Definition\ObjectType;

class ClassesTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Classes'
        ];
    }

    public function fields()
    {

        $dataType = new ObjectType([
            'name' => 'dataClasses',
            'fields' => [
                'label' => ['type' => Type::string()],
                'value' => ['type' => Type::string()],
                'color' => ['type' => Type::string()]
            ]
        ]);

        $reportType = new ObjectType([
            'name' => 'reportClasses',
            'fields' => [
                'Data' => ['type' => Type::listOf($dataType)],
                'Title' => ['type' => Type::string()]
            ]
        ]);

        $submenuType = new ObjectType([
            'name' => 'submenuClasses',
            'fields' => [
                'label' => ['type' => Type::string()],
                'url' => ['type' => Type::string()],
                'type' => ['type' => Type::string()],
                'icon' => ['type' => Type::string()]
            ]
        ]);

        $urlType = new ObjectType([
            'name' => 'urlClasses',
            'fields' => [
                'label' => ['type' => Type::string()],
                'url' => ['type' => Type::string()],
                'type' => ['type' => Type::string()],
                'icon' => ['type' => Type::string()],
                'color' => ['type' => Type::string()],
                'submenu' => ['type' => Type::listOf($submenuType)]
            ]
        ]);

        $headerClassesType = new ObjectType([
            'name' => 'headerClasses',
            'fields' => [
                'Report' => [
                    'type' => Type::listOf($reportType)
                ],
                'WelcomeMessage' => [
                    'type' => Type::string()
                ],
                'urls' => [
                    'type' => Type::listOf($urlType)
                ],
                'Title' => ['type' => Type::string()],
                'icon' => ['type' => Type::string()]
            ]
        ]);
        return [
            'Header' => ['type' => $headerClassesType]
        ];
    }
}
