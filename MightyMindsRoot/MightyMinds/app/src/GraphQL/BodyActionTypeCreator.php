<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class BodyActionTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'BodyAction',
        ];
    }

    public function fields()
    {
        return [
            'title' => ['type' => Type::string()],
            'text' => ['type' => Type::string()],
            'icon' => ['type' => Type::string()],
            'action' => ['type' => Type::string()],
        ];
    }
}
