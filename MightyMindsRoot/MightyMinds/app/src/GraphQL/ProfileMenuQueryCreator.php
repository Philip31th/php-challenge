<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class ProfileMenuQueryCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'ProfileMenu',
        ];
    }

    public function type()
    {
        return Type::listOf($this->manager->getType('profilemenu'));
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {

        $data = [
            [
                'label' => "My Profile",
                'url' => "/my-profile",
                'type' => "href",
                'icon' => "arrow"
            ],
            [
                'label' => "My School's Details",
                'url' => "/my-school-details",
                'type' => "href",
                'icon' => "arrow"
            ],
            [
                'label' => "Licence Agreement",
                'url' => "/licence-agreement",
                'type' => "href",
                'icon' => "arrow"
            ],
            [
                'label' => "",
                'url' => "/",
                'type' => "separator",
                'icon' => ""
            ],
            [
                'label' => "Change Password",
                'url' => "/my-profile",
                'type' => "href",
                'icon' => "password"
            ],
            [
                'label' => "Logout",
                'url' => "/logout",
                'type' => "href",
                'icon' => "logout"
            ],
            [
                'label' => "Policy Privacy",
                'url' => "/policy-privacy",
                'type' => "href",
                'icon' => "arrow"
            ],

        ];

        return $data;
    }
}
