<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class ModalsQueryCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'Modals',
        ];
    }

    public function type()
    {
        return $this->manager->getType('modals');
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {

        $data = [
            'Header' => [
                'WelcomeMessage' => 'Test',
                'urls' => [
                    [
                        'label' => "home",
                        'url' => "/",
                        'type' => "href",
                        'icon' => "",
                    ],
                    [
                        'label' => "Classes",
                        'url' => "classes",
                        'type' => "href",
                        'icon' => "",
                    ],
                    [
                        'label' => "Planner",
                        'url' => "planner",
                        'type' => "href",
                        'icon' => "",
                        'submenu' => [
                            [
                                'label' => "Calendar",
                                'url' => "calendar",
                                'type' => "href",
                                'icon' => "calendar",
                            ],
                            [
                                'label' => "Activities",
                                'url' => "activities",
                                'type' => "href",
                                'icon' => "activities",
                            ],
                            [
                                'label' => "Programs",
                                'url' => "programs",
                                'type' => "href",
                                'icon' => "programs",
                            ],
                            [
                                'label' => "Unit Plans",
                                'url' => "unit-plans",
                                'type' => "href",
                                'icon' => "plans",
                            ],
                        ]
                    ],
                ],
                'Report' => [
                    [
                        'Title' => 'Week 4 Activity Summary',
                        'Data' => [
                            [
                                'label' => "Due This week",
                                'value' => 330,
                                'color' => "blue"
                            ],
                            [
                                'label' => "Completed",
                                'value' => 240,
                                'color' => "green"
                            ],
                            [
                                'label' => "Overdue",
                                'value' => 33,
                                'color' => "red",
                            ]
                        ]
                    ]
                            ],
                'Title' => '',
                'icon' => ''
            ]
        ];

        return $data;
    }
}
