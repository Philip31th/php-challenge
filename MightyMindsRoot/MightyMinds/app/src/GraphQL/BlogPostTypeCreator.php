<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;
use GraphQL\Type\Definition\ObjectType;

class BlogPostTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'blogPost',
            'description' => 'Add a description here'
        ];
    }

    public function fields()
    {
        return [
            'Title' => ['type' => Type::string()],
            'Content' => ['type' => Type::string()],
        ];
    }
}