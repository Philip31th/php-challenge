<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class DataTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Data',
            'description' => 'Add a description here'
        ];
    }

    public function fields()
    {
        return [
            'label' => ['type' => Type::string()],
            'value' => ['type' => Type::string()],
            'color' => ['type' => Type::string()]
        ];
    }
}