<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class MainTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Main'
        ];
    }

    public function fields()
    {
        $submenu = new SubMenuTypeCreator();
        $MainSubmenuType = new ObjectType([
            'name' => 'MainSubmenu',
            'fields' => $submenu->getFields()
        ]); 
        return [
            'label' => ['type' => Type::string()],
            'url' => ['type' => Type::string()],
            'type' => ['type' => Type::string()],
            'icon' => ['type' => Type::string()],
            'submenu' => ['type' => Type::listOf($MainSubmenuType)]
        ];
    }
}