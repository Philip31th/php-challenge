<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class TopMenuTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'TopMenu'
        ];
    }

    public function fields()
    {
        $main = new MainTypeCreator();
        $mainType = new ObjectType($main->toArray());

        $help = new HelpTypeCreator();
        $helpType = new ObjectType($help->toArray());
        return [
            'main' => ['type' => Type::listOf($mainType)],
            'help' => ['type' => Type::listOf($helpType)],
        ];
    }
}
