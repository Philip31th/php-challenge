<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class UrlTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'url'
        ];
    }

    public function fields()
    {
        $submenu = new SubMenuTypeCreator();
        $submenuType = new ObjectType($submenu->toArray());
        return [
            'label' => ['type' => Type::string()],
            'url' => ['type' => Type::string()],
            'type' => ['type' => Type::string()],
            'icon' => ['type' => Type::string()],
            'color' => ['type' => Type::string()],
            'submenu' => ['type' => Type::listOf($submenuType)]
        ];
    }
}