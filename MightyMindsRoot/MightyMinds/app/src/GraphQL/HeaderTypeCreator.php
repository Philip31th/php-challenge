<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class HeaderTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Header'
        ];
    }

    public function fields()
    {
        // type = url 
        $url = new UrlTypeCreator();
        $urlType = new ObjectType($url->toArray());

        // type = report
        $report = new ReportTypeCreator();
        $reportType = new ObjectType($report->toArray());        

        return [
            'Report' => [
                'type' => Type::listOf($reportType)
            ],
            'WelcomeMessage' => [
                'type' => Type::string()
            ],
            'urls' => [
                'type' => Type::listOf($urlType)
            ],
            'Title' => ['type' => Type::string()],
            'icon' => ['type' => Type::string()]
        ];
    }
}