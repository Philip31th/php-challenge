<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class LinksTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Links'
        ];
    }

    public function fields()
    {
        return [
            'Label' => ['type' => Type::string()],
            'Url' => ['type' => Type::string()],
            'Type' => ['type' => Type::string()],
            'Icon' => ['type' => Type::string()],
            'Color' => ['type' => Type::string()]
        ];
    }
}