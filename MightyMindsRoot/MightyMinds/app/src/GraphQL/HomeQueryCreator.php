<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use MightyMinds\Model\Links;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class HomeQueryCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'Home',
        ];
    }

    public function type()
    {
        return $this->manager->getType('home');
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {
        $urls = [
            [
                'type' => "button",
                'label' => "My Calendar",
                'url' => "/my-calendar",
                'icon' => "plus",
                'color' => "secondary",
            ],
            [
                'type' => "button",
                'label' => "Weekly Report",
                'url' => "/weekly-report",
                'icon' => "plus",
                'color' => "secondary",

            ],
            [
                'type' => "button-submenu",
                'label' => "Assign Activity",
                'url' => "/assign-activity",
                'icon' => "plus",
                'color' => "primary",
                'submenu' => [
                    [
                        'label'=> "Assign Activity",
                        'url'=> "/my-profile",
                        'type'=> "href",
                        'icon'=> "arrow"
                    ],
                    [
                        'label'=> "Start a Live Challenge",
                        'url'=> "/my-school-details",
                        'type'=> "href",
                        'icon'=> "arrow",
                    ],
                    [
                        'label'=> "Present Activities and Theory",
                        'url'=> "/licence-agreement",
                        'type'=> "href",
                        'icon'=> "arrow",
                    ],
                    [
                        'label'=> "Request Activities",
                        'url'=> "/policy-privacy",
                        'type'=> "href",
                        'icon'=> "arrow",
                    ]
                ]
            ]
        ];

        $reports = [
            [
                'Title' => 'Week 4 Activity Summary',
                'Data' => [
                    [
                        'label' => "Due This week",
                        'value' => 330,
                        'color' => "blue"
                    ],
                    [
                        'label' => "Completed",
                        'value' => 240,
                        'color' => "green"
                    ],
                    [
                        'label' => "Overdue",
                        'value' => 33,
                        'color' => "red",
                    ]
                ]
            ]
        ];


        $header = [
            'Report' => $reports,
            'WelcomeMessage' => "Welcome Back, ",
            'urls' => $urls
        ];
        $data = [
            'urls' => $urls,
            'Header' => $header
        ];

        return $data;
    }
}
