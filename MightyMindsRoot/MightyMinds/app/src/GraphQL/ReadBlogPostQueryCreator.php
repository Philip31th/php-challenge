<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use MightyMinds\Model\Links;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use SilverStripe\GraphQL\QueryCreator;

class ReadBlogPostQueryCreator extends QueryCreator implements OperationResolver
{
    public function attributes()
    {
        return [
            'name' => 'readBlogPost'
        ];
    }

    public function type()
    {
        return Type::listOf($this->manager->getType('blogPost'));
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {
        $list = BlogPost::get();
/*         $list = array(
            array(
                'Title' => 'test',
                'Content' => 'content',
                'Datatype' => array(
                    array(
                        'label' => 'test_label',
                        'value' => 'test_value',
                        'color' => 'test_color'
                    )
                )
            )
        ); */

        // Optional filtering by properties
        /* if (isset($args['Title'])) {
            $list = $list->filter('Title', $args['Title']);
            
        } */

        return $list;
    }
}