<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class TeachersTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Teachers'
        ];
    }

    public function fields()
    {
        return [
            'ID' => ['type' => Type::int()],
            'lastname' => ['type' => Type::string()],
            'firstname' => ['type' => Type::string()],
        ];
    }
}
