<?php

namespace MightyMinds\GraphQL;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class ReportTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Report'
        ];
    }

    public function fields()
    {
        $data = new DataTypeCreator();
        $dataType = new ObjectType($data->toArray());

        return [
            'Title' => [
                'type' => Type::string()
            ],
            'Data' => ['type' => Type::listOf($dataType)]
        ];
    }
}
