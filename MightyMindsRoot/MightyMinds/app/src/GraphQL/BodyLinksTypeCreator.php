<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class WidgetUrlsTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'WidgetURLS',
            'description' => 'Add a description here'
        ];
    }

    public function fields()
    {
        return [
            'label' => ['type' => Type::string()],
            'url' => ['type' => Type::string()],
            'type' => ['type' => Type::string()],
            'color' => ['type' => Type::string()]
        ];
    }
}