<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use MightyMinds\Model\Links;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class QueryTestCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'Query',
        ];
    }

    public function type()
    {
        return Type::listOf($this->manager->getType('links'));
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {
        $list = Links::get();
        return null;
    }
}