<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class CreateNewClassesTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'CreateNewClasses',
        ];
    }

    public function fields()
    {
        return [
            'dropdowns' => ['type' => Type::string()],
            'title' => ['type' => Type::string()],
            'buttons' => ['type' => Type::string()],
        ];
    }
}
