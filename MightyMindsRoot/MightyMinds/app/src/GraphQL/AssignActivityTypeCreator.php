<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class AssignActivityTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'AssignActivity',
        ];
    }

    public function fields()
    {
        return [
            'title' => ['type' => Type::string()],
            'bodyText' => ['type' => Type::string()],
            'bodyActions' => ['type' => Type::string()]
        ];
    }
}
