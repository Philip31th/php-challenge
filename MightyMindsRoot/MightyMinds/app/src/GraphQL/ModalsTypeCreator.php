<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class ModalsTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Modals',
        ];
    }

    public function fields()
    {
        $createNewClass = new CreateNewClassesTypeCreator();
        $createNewClassType = new ObjectType($createNewClass->toArray());
        return [
            'createNewClasses' => ['type' => $createNewClassType],
            'assignActivity' => ['type' => Type::string()],
        ];
    }
}