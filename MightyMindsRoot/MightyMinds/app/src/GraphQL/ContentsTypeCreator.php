<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;
use GraphQL\Type\Definition\ObjectType;

class ContentsTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Contents'
        ];
    }

    public function fields()
    {
        $header = new HeaderTypeCreator();
        $headerType = new ObjectType($header->toArray());
        //$contents = new ContentsTypeCreator();
        //$contentType = new ObjectType($contents->toArray());
        return [
            'Header' => [
                'type' => $headerType,
                'resolve' => function($data){
                    return $data['Header'];
                }
            ]
        ];
    }
}