<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class ContextMenuTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'ContextMenu',
            'description' => 'Add a description here'
        ];
    }

    public function fields()
    {
        return [
            'label' => ['type' => Type::string()],
            'url' => ['type' => Type::string()],
            'type' => ['type' => Type::string()],
            'icon' => ['type' => Type::string()],
            'color' => ['type' => Type::string()]
        ];
    }
}