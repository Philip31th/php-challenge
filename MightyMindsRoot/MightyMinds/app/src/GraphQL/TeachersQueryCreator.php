<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class TeachersQueryCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'Teachers',
        ];
    }

    public function type()
    {
        return Type::listOf($this->manager->getType('teachers'));
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {

        $data = [
            [
                'ID' => "51844",
                'firstname' => "Professor",
                'lastname' => "Rutigliano",
            ], [
                'ID' => "51845",
                'firstname' => "Professor",
                'lastname' => "Mastrorillo",
            ]
        ];

        return $data;
    }
}
