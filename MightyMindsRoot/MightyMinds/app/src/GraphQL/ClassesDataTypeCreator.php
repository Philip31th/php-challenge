<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;
use GraphQL\Type\Definition\ObjectType;
use MightyMinds\Model\ContextMenu;

class ClassesDataTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'classesdata'
        ];
    }

    public function fields()
    {
        return [
            'ClassesDetails' => [
                'type' => $this->manager->getType('classesdetails')
            ]
        ];
    }
}
