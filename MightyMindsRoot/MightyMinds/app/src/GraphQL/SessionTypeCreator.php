<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class SessionTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Session'
        ];
    }

    public function fields()
    {
        $users = new UserTypeCreator();
        $usersType = new ObjectType($users->toArray());

        $schoolSubscriptions = new SchoolSubcriptionsTypeCreator();
        $schoolSubscriptionsType = new ObjectType($schoolSubscriptions->toArray());

        return [
            'isTeacher' => ['type' => Type::boolean()],
            'user' => ['type' => $usersType],
            'schoolSubscriptions' => ['type' => $schoolSubscriptionsType],
            'csrf' => ['type' => Type::string()],
            'schoolCourses' => ['type' => Type::boolean()],
            'message' => ['type' => Type::string()],
            'isAcademicForce' => ['type' => Type::boolean()],
            'redirectUrl' => ['type' => Type::string()],
        ];
    }
}
