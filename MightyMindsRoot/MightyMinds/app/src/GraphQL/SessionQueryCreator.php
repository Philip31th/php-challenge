<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use MightyMinds\Model\Links;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class SessionQueryCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'Session',
        ];
    }

    public function type()
    {
        return $this->manager->getType('session');
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {

        $schoolSubscriptions = [
            'current' => ["All", "Cognitive Verbs", "Fundamental Skills"],
            'all' => ["All", "Cognitive Verbs", "Fundamental Skills"]
        ];

        $user = [
            'FirstName' => "Professor",
            'Surname' => "Rutigliano",
            'Email' => "rutigliano.teacher@mightyminds.com.au",
            'SchoolName' => "Old Clever School",
            'ID' => 51844,
            'SchoolID' => 1709,
            'IsSchoolAdmin' => false
        ];
        $data = [
            'isTeacher' => true,
            'user' => $user,
            'schoolSubscriptions' => $schoolSubscriptions,
            'csrf' => '079d945f27e94a4417231f46cda0985559a3fa20',
            'schoolCourses' => true,
            'message' => "",
            'isAcademicForce' => false,
            'redirectUrl' => "/login",
        ];

        return $data;
    }
}
