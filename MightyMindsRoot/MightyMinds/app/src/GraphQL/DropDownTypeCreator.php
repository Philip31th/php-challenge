<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class DropDownTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'dropdown',
            'description' => 'Add a description here'
        ];
    }

    public function fields()
    {
        $yearType = new ObjectType([
            'name' => 'Year',
            'fields' => [
                'option' => ['type' => Type::string()],
                'value' =>  ['type' => Type::string()]

            ]
        ]);
        $yearType = new ObjectType([
            'name' => 'Subject',
            'fields' => [
                'option' => ['type' => Type::string()],
                'value' =>  ['type' => Type::string()]

            ]
        ]);        
        return [
            'year' => ['type' => Type::listOf($yearType)],
            'subjects' => ['type' => Type::listOf($yearType)],
        ];
    }
}
