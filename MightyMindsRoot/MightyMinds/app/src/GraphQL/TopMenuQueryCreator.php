<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class TopMenuQueryCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'TopMenu',
        ];
    }

    public function type()
    {
        return $this->manager->getType('topmenu');
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {

        $data = [
            'main' => [
                [
                    'label' => "home",
                    'url' => "/",
                    'type' => "href",
                    'icon' => "",
                ],
                [
                    'label' => "Classes",
                    'url' => "classes",
                    'type' => "href",
                    'icon' => "",
                ],
                [
                    'label' => "Planner",
                    'url' => "planner",
                    'type' => "href",
                    'icon' => "",
                    'submenu' => [
                        [
                            'label' => "Calendar",
                            'url' => "calendar",
                            'type' => "href",
                            'icon' => "calendar",
                        ],
                        [
                            'label' => "Activities",
                            'url' => "activities",
                            'type' => "href",
                            'icon' => "activities",
                        ],
                        [
                            'label' => "Programs",
                            'url' => "programs",
                            'type' => "href",
                            'icon' => "programs",
                        ],
                        [
                            'label' => "Unit Plans",
                            'url' => "unit-plans",
                            'type' => "href",
                            'icon' => "plans",
                        ],
                    ]
                ],
                [
                    'label' => "School Data",
                    'url' => "schooldata",
                    'type' => "href",
                    'icon' => "",
                ],
                [
                    'label' => "Library",
                    'url' => "library",
                    'type' => "href",
                    'icon' => "",
                ]
                ],
            'help' =>[
                [
                    'label' => "Help Cente",
                    'url' => "helpcenter",
                    'type' => "href",
                    'icon' => "help",
                ]
            ]
        ];

        return $data;
    }
}
