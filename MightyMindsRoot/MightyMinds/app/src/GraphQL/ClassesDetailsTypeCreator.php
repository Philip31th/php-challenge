<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;
use GraphQL\Type\Definition\ObjectType;
use MightyMinds\Model\ContextMenu;

class ClassesDetailsTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'ClassesDetails'
        ];
    }

    public function fields()
    {
        $infoBarType = new ObjectType([
            'name' => 'infobar',
            'fields' => [
                'icon' => ['type' => Type::string()],
                'year' => ['type' => Type::string()],
                'classname' => ['type' => Type::string()]
            ]
        ]);

        $dataType = new ObjectType([
            'name' => 'ClassData',
            'fields' => [
                'SchoolID' => ['type' => Type::string()],
                'ClassID' => ['type' => Type::string()],
                'Title' => ['type' => Type::string()],
                'icon' => ['type' => Type::string()],
                'color' => ['type' => Type::string()],
                'starred' => ['type' => Type::boolean()],
                'archived' => ['type' => Type::boolean()],
                'infobar' => ['type' => $infoBarType],
                'WeekActivities' => ['type' => Type::string()],
            ]
        ]);

        $bottomLinks = new BottomLinksTypeCreator();
        $bottomLinksType = new ObjectType($bottomLinks->toArray());

        $contextMenu = new ContextMenuTypeCreator();
        $contextMenuType = new ObjectType($contextMenu->toArray());


        $urlsType = new ObjectType([
            'name' => 'classesDataUrl',
            'fields' => [
                'label' => ['type' => Type::string()],
                'url' => ['type' => Type::string()],
                'type' => ['type' => Type::string()],
                'icon' => ['type' => Type::string()],
                'color' => ['type' => Type::string()]
            ]
        ]);


        $classMenusType = new ObjectType([
            'name' => 'classMenus',
            'fields' => [
                'urls' => ['type' => Type::listOf($urlsType)],
                'bottomLinks' => ['type' => Type::listOf($bottomLinksType)],
                'ContextMenu' => ['type' => Type::listOf($contextMenuType)]
            ]
        ]);

        return [
            'classMenus' => [
                'type' => $classMenusType
            ],
            'data' => [
                'type' => Type::listOf($dataType)
            ]
        ];
    }
}
