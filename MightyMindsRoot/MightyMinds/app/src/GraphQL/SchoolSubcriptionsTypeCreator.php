<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class SchoolSubcriptionsTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'schoolSubscriptions'
        ];
    }

    public function fields()
    {
        return [
            'current' => ['type' => Type::listOf(Type::string())],
            'all' => ['type' => Type::listOf(Type::string())]
        ];
    }
}
