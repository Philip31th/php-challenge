<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\Security\Member;
use SilverStripe\GraphQL\OperationResolver;
use GraphQL\Type\Definition\ObjectType;
use SilverStripe\GraphQL\QueryCreator;

class ClassesDataQueryCreator extends QueryCreator implements OperationResolver
{

    public function attributes()
    {
        return [
            'name' => 'ClassesData',
        ];
    }

    public function type()
    {
        return $this->manager->getType('classesdata');
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {

        $data = [
            'ClassesDetails' => [
                'classMenus' => [
                    'urls' => [
                        [
                            'label' => "Activities due this week",
                            'url' => "week-activities",
                            'type' => "href-withbadge",
                            'icon' => "arrow",
                            'color' => ""
                        ],
                        [
                            'label' => "Assign Activities",
                            'url' => "assign-activities",
                            'type' => "href",
                            'icon' => "arrow",
                            'color' => "color"
                        ],
                        [
                            'label' => "Class Calendar",
                            'url' => "class-calendar",
                            'type' => "href",
                            'icon' => "arrow",
                            'color' => "color"
                        ],
                        [
                            'label' => "",
                            'url' => "/",
                            'type' => "separator",
                            'icon' => "arrow",
                            'color' => ""
                        ]
                    ],
                    'bottomLinks' => [
                        [
                            'label' => "Add Student",
                            'url' => "/add-student",
                            'type' => "modal-trigger",
                            'icon' => "plus",
                            'color' => ""
                        ],
                    ],
                    'contextMenu' => [
                        [
                            'label' => "Add Student",
                            'url' => "/add-student",
                            'type' => "modal-trigger",
                            'icon' => "plus",
                            'color' => ""
                        ],
                        [
                            'label' => "Email Students",
                            'url' => "/email-students",
                            'type' => "href",
                            'icon' => "arrow",
                            'color' => ""
                        ],
                        [
                            'label' => "Download Login Instructions",
                            'url' => "/download-login-instructions",
                            'type' => "href",
                            'icon' => "arrow",
                            'color' => ""
                        ],
                        [
                            'label' => "Login as a student",
                            'url' => "/login-as-a-student",
                            'type' => "separator",
                            'icon' => "arrow",
                            'color' => ""
                        ],
                        [
                            'label' => "",
                            'url' => "",
                            'type' => "separator",
                            'icon' => "arrow",
                            'color' => ""
                        ],
                        [
                            'label' => "Rename Class",
                            'url' => "/rename-class",
                            'type' => "href",
                            'icon' => "",
                            'color' => ""
                        ],
                        [
                            'label' => "Change your level",
                            'url' => "/change-your-level",
                            'type' => "href",
                            'icon' => "",
                            'color' => ""
                        ],
                        [
                            'label' => "View class subscription",
                            'url' => "/view-class-subscription",
                            'type' => "href",
                            'icon' => "",
                            'color' => ""
                        ],
                        [
                            'label' => "View class unit plan",
                            'url' => "view-class-unit-plan",
                            'type' => "href",
                            'icon' => "",
                            'color' => ""
                        ],
                        [
                            'label' => "Archive Class",
                            'url' => "/archive-class",
                            'type' => "modal-trigger",
                            'icon' => "",
                            'color' => ""
                        ]
                    ],
                ],
                'data' => [
                    [
                        'SchoolID' => 89309,
                        'ClassID' => 456231,
                        'Title' => "12ENGA",
                        'icon' => "classicon",
                        'color' => "#6BC62A",
                        'starred' => true,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 5,
                    ],
                    [
                        'SchoolID' => 89310,
                        'ClassID' => 456232,
                        'Title' => "12ENGB",
                        'icon' => "classicon",
                        'color' => "#487DF6",
                        'starred' => true,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 3,
                    ],
                    [
                        'SchoolID' => 89311,
                        'ClassID' => 456233,
                        'Title' => "08MATHS",
                        'icon' => "classicon",
                        'color' => "#F7AD33",
                        'starred' => true,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 0,
                    ],
                    [
                        'SchoolID' => 89312,
                        'ClassID' => 456234,
                        'Title' => "09SCI",
                        'icon' => "classicon",
                        'color' => "#EF408B",
                        'starred' => true,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 1,
                    ],
                    [
                        'SchoolID' => 89313,
                        'ClassID' => 456235,
                        'Title' => "09HASS",
                        'icon' => "classicon",
                        'color' => "#6A44D2",
                        'starred' => false,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 2,
                    ],
                    [
                        'SchoolID' => 89314,
                        'ClassID' => 456236,
                        'Title' => "12ENGA",
                        'icon' => "classicon",
                        'color' => "#F7AD33",
                        'starred' => false,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 6,
                    ],
                    [
                        'SchoolID' => 89315,
                        'ClassID' => 456237,
                        'Title' => "12ENGA",
                        'icon' => "classicon",
                        'color' => "#487DF6",
                        'starred' => true,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 1,
                    ],
                    [
                        'SchoolID' => 89313,
                        'ClassID' => 456235,
                        'Title' => "09HASS",
                        'icon' => "classicon",
                        'color' => "#6A44D2",
                        'starred' => false,
                        'archived' => false,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 2,
                    ],
                    [
                        'SchoolID' => 89308,
                        'ClassID' => 456238,
                        'Title' => "12ENGA",
                        'icon' => "classicon",
                        'color' => "#6BC62A",
                        'starred' => true,
                        'archived' => true,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 5,
                    ],
                    [
                        'SchoolID' => 89307,
                        'ClassID' => 456239,
                        'Title' => "12ENGB",
                        'icon' => "classicon",
                        'color' => "#487DF6",
                        'starred' => true,
                        'archived' => true,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 3,
                    ],
                    [
                        'SchoolID' => 89306,
                        'ClassID' => 456240,
                        'Title' => "08MATHS",
                        'icon' => "classicon",
                        'color' => "#F7AD33",
                        'starred' => true,
                        'archived' => true,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 0,
                    ],
                    [
                        'SchoolID' => 89305,
                        'ClassID' => 456241,
                        'Title' => "09SCI",
                        'icon' => "classicon",
                        'color' => "#EF408B",
                        'starred' => true,
                        'archived' => true,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 1,
                    ],
                    [
                        'SchoolID' => 89304,
                        'ClassID' => 456242,
                        'Title' => "09HASS",
                        'icon' => "classicon",
                        'color' => "#6A44D2",
                        'starred' => false,
                        'archived' => true,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 2,
                    ],
                    [
                        'SchoolID' => 89303,
                        'ClassID' => 456243,
                        'Title' => "12ENGA",
                        'icon' => "classicon",
                        'color' => "#F7AD33",
                        'starred' => false,
                        'archived' => true,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 6,
                    ],
                    [
                        'SchoolID' => 89302,
                        'ClassID' => 456244,
                        'Title' => "12ENGA",
                        'icon' => "classicon",
                        'color' => "#487DF6",
                        'starred' => true,
                        'archived' => true,
                        'infobar' => [
                            'icon' => "star",
                            'year' => "Year 12",
                            'classname' => "English",
                        ],
                        'WeekActivities'=> 1,
                    ],

                ]
            ],
        ];

        return $data;
    }
}
