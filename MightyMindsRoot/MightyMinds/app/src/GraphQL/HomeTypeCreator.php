<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class HomeTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Home'
        ];
    }

    public function fields()
    {
        $contents = new ContentsTypeCreator();
        $contentType = new ObjectType($contents->toArray());
        return [
            'Contents' => [
                'type' => $contentType,
                'resolve' => function($data){
                    return $data;
                }
            ]
        ];
    }

}