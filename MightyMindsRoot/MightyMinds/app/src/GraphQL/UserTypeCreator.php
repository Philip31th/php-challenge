<?php

namespace MightyMinds\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class UserTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'User',
            'description' => 'Add a description here'
        ];
    }

    public function fields()
    {
        return [
            'FirstName' => ['type' => Type::string()],
            'Surname' => ['type' => Type::string()],
            'Email' => ['type' => Type::string()],
            'SchoolName' => ['type' => Type::string()],
            'ID' => ['type' => Type::int()],
            'SchoolID' => ['type' => Type::int()],
            'IsSchoolAdmin' => ['type' => Type::int()] 
        ];
    }
}
