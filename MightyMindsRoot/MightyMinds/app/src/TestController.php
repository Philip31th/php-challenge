<?php

use MightyMinds\GraphQL\BlogPost;
use MightyMinds\Model\Links;
use MightyMinds\Model\SidebarWidgets;
use SilverStripe\ORM\ArrayList;



class TestController extends \SilverStripe\Control\Controller
{
  private static $url_handlers = [
    'staff/$@' => 'index',
  ];

  public function index($request)
  {
    // GET /staff/managers/bob
    //$request->latestParam('$1'); // managers
    //$request->latestParam('$2'); // bob
    $list1 = Links::get();
    $list = array(
      array(
        'Title' => 'test',
        'Content' => 'content',
        'Datatype' => array(
          array(
            'label' => 'test_label',
            'value' => 'test_value',
            'color' => 'test_color'
          )
        )
      )
    );
    //$list1['data'] = $list1; 
    $data = json_encode($list1);
    return $list1;
    //echo json_encode($list1);
    //echo json_decode($data[0]);
  }
}
