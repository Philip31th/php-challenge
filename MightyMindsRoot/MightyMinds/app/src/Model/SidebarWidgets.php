<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class SidebarWidgets extends DataObject
{
  private static $db = [
    "Label" => "Varchar(255)",
    "Url" => "Varchar(255)",
    "Type" => "Varchar(255)",
    "Icon" => "Varchar(255)",
    "Color" => "Varchar(255)",
  ];
  
  private static $has_many = [
    'Links' => Links::class
  ];
  
  private static $table_name = "SidebarWidgets";
}
