<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class BodyLinks extends DataObject
{
  private static $db = [
    "Label" => "Varchar(255)",
    "Url" => "Varchar(255)",
    "Icon" => "Varchar(255)"
  ];

  private static $table_name = "BodyLinks";
}
