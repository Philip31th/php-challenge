<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class Help extends DataObject
{
  private static $db = [
    "Label" => "Varchar(255)",
    "Url" => "Varchar(255)",
    "Type" => "Varchar(255)",
    "Icon" => "Varchar(255)",
  ];

  private static $table_name = "Help";
}
