<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class BodyAction extends DataObject
{
  private static $db = [
    "title" => "Varchar(255)",
    "text" => "Varchar(255)",
    "icon" => "Varchar(255)",
    "action" => "Varchar(255)"
  ];

  private static $table_name = "BodyAction";
}
