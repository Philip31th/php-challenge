<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class User extends DataObject
{
  private static $db = [
    "Firstname" => "Varchar(255)",
    "Surname" => "Varchar(255)",
    "Email" => "Varchar(255)",
    "Schoolname" => "Varchar(255)",
    "SchoolId" => "Int",
    "IsSchoolAdmin" => "Boolean",   
  ];

  private static $table_name = "User";
}
