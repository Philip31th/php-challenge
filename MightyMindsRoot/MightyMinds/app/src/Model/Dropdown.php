<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class Dropdown extends DataObject
{
  private static $db = [
    "Option" => "Varchar(255)",
    "Value" => "Varchar(255)",
  ];

  private static $table_name = "Dropdown";
}
