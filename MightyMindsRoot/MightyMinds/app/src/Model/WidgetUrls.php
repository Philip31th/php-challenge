<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class WidgetUrls extends DataObject
{
  private static $db = [
    "type" => "Varchar(255)",
    "Label" => "Varchar(255)",
    "Url" => "Varchar(255)",
    "Icon" => "Varchar(255)"
  ];

  private static $table_name = "WidgetUrls";
}
