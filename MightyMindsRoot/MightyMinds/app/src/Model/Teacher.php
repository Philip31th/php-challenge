<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class Teacher extends DataObject
{
  private static $db = [
    // "Id" => "Int",
    "Lastname" => "Varchar(255)",
    "Firstname" => "Varchar(255)"
  ];

  private static $table_name = "Teacher";
}
