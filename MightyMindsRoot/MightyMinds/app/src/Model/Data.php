<?php

namespace MightyMinds\Model;

use SilverStripe\ORM\DataObject;

class Data extends DataObject
{
  private static $db = [
    "Label" => "Varchar(255)",
    "Value" => "Varchar(255)",
    "Color" => "Varchar(255)"
  ];

  private static $table_name = "Data";
}
